package com.example.demo.controller;

import com.example.demo.entity.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class ThymeleafElController {
    @RequestMapping("/varEL")
    public String varEL(Model model, HttpSession session){
        model.addAttribute("result",100+200);
        Student student01 = new Student();
        student01.setName("Jeff");
        student01.setPhoneNumber("18039670878");
        model.addAttribute("student01",student01);
        Student student02 = new Student();
        student02.setName("Jack");
        student02.setAddress("河南财经政法大学");
        session.setAttribute("student02",student02);
        return "thymeleaf_EL";
    }


}
