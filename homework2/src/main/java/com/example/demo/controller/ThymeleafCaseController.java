package com.example.demo.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ThymeleafCaseController {
    @RequestMapping("/")
    public String toCase(Model model){
        model.addAttribute("message","Hello,how are you doing?");
        return "thymeleafCase";
    }

}
