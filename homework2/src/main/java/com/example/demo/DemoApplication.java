package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.util.Objects;

@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context =SpringApplication.run(DemoApplication.class, args);
        Environment environment =context.getEnvironment();
        String host = "localhost";
        int port = Integer.parseInt(Objects.requireNonNull(environment.getProperty("server.port")));
        String rootPath = environment.getProperty("server.servlet.context-path", "/");
        String url = String.format("访问根路径的地址信息: http://%s:%d%s", host, port, rootPath);
        System.out.println(url);
    }

}
