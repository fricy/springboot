package com.example.springbootdemo.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
public class RandomController {

    @GetMapping("/random")
    public int getRandomNumber() {
        Random random = new Random();
        return random.nextInt(1000);
    }
}
